pub const GRASS_GRID_COLUMNS: usize = 3;
pub const GRASS_GRID_ROWS: usize = 3;
pub const GRASS_PATH: &str = "grass.png";
/// Sand asset columns quantity.
pub const SAND_GRID_COLUMNS: usize = 0;
/// Sand asset rows quantity.
pub const SAND_GRID_ROWS: usize = 0;
/// Sand asset file path.
pub const SAND_PATH: &str = "sand.png";
pub const WATER_GRID_COLUMNS: usize = 0;
pub const WATER_GRID_ROWS: usize = 0;
pub const WATER_PATH: &str = "water.png";
