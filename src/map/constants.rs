pub mod assets;
pub mod chunks;
pub mod tiles;

/// The scale of the noise map
/// 
/// This is effectively how zoomed in into the noise map.
pub const SCALE: f64 = 0.025;
