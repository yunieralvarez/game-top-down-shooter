pub mod constants {
	pub const PLAYER_RENDER_Z_INDEX: f32 = 1.0;
	/// The radius, in chunks, around the player within which chunks will be rendered.
	pub const CHUNKS_RENDER_RADIUS: f32 = 4.0;
	pub const TERRAIN_RENDER_Z_INDEX: f32 = 0.0;
}
